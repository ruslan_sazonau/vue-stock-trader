import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';

import App from './App.vue';
import { routes } from './routes.js';
import store from './components/store/store.js';

import 'bootstrap/dist/css/bootstrap.min.css';

Vue.config.productionTip = false;

Vue.use(VueRouter);
Vue.use(VueResource);

Vue.http.options.root = 'https://vuejs-stock-trader-8cfdc.firebaseio.com/';

Vue.filter('currency', function (value) {
    return '$' + value.toLocaleString();
});

const router = new VueRouter({
    routes: routes,
    mode: 'history'
});

new Vue({
    render: h => h(App),
    router: router,
    store: store
}).$mount('#app');
