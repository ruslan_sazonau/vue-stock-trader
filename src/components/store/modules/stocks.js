const state = {
    stocks: []
};

const mutations = {
    'SET_STOCKS': function (state, stocks) {
        state.stocks = stocks;
    },
    'RND_STOCKS': function (state) {
        state.stocks.forEach(stock => {
            stock.price = Math.round(stock.price * (1 + Math.random()));
        });
    }
};

const actions = {
    buyStock: function ({commit}, order) {
        commit('BUY_STOCK', order);
    },
    initStocks: function ({commit}) {
        commit('SET_STOCKS', [
            { id: 1, name: 'Stock1', price: 1 },
            { id: 2, name: 'Stock2', price: 2 },
            { id: 3, name: 'Stock3', price: 3 }
        ]);
    },
    randomizeStocks: function ({commit}) {
        commit('RND_STOCKS');
    }
};

const getters = {
    stocks: function () {
        return state.stocks;
    }
};

export default {
    state: state,
    mutations: mutations,
    actions: actions,
    getters: getters
};